﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rppoon
{
    class Program
    {
        static void Main(string[] args)
        {
            Note firstNote = new Note();
            Note secondNote = new Note("Hello", "Adrian Zuparic", 2);
            Note thirdNote = new Note("Hi!", "Empty");

            Console.WriteLine(firstNote.getNote());
            Console.WriteLine(secondNote.getNote());
            Console.WriteLine(thirdNote.getNote());

            Console.WriteLine(firstNote.getAuthorName());
            Console.WriteLine(secondNote.getAuthorName());
            Console.WriteLine(thirdNote.getAuthorName());


            Console.WriteLine(firstNote.ToString());
            Console.WriteLine(secondNote.ToString());
            Console.WriteLine(thirdNote.ToString());
        }
    }
}
